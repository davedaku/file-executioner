# file-executioner

Randomly deletes contents of a directory until the directory is below a specified size (in GB)

## Usage
`executioner.ps1 'A:\truckMusic' -limitGb 29`

Will delete files from A:\truckMusic until that directory's total size is less than 29 GB

### Or

`executioner.ps1 'A:\truckMusic' -limitGb 29 -move 'A:\purgatory'`

Will move files from A:\truckMusic to A:\purgatory, until the total size of A:\truckMusic is less than 29 GB


