param (
    [Parameter(Mandatory=$true)][System.IO.FileInfo]$dirPath,
    [int]$limitGb,
    [System.IO.FileInfo]$move
)

$dir = Get-Item $dirPath

# look at directory, store size
$currSize = ($dir | Get-ChildItem | Measure-Object -Sum Length).Sum / 1GB
$opsSinceMeasurement = 0

Write-Host "Directory '$dirPath' is currently $currSize GB , and must become less than $limitGb GB."

while ($limitGb -LT $currSize) {    

    # find a random file
    $file = $dir | Get-ChildItem | Get-Random
    $fileSize = ($file | Measure-Object -Sum Length).Sum / 1GB
    #$fileName = $file.Name
    
    # subtract size
    $currSize -= $fileSize
    $opsSinceMeasurement += 1

    # move or delete the file
    if ($move) {
        Move-Item $file -Destination $move
    } else {
        Remove-Item $file
    }

    # if out of date, re-check
    if ($opsSinceMeasurement -GT 10) {
        # look at directory, store size
        $currSize = ($dir | Get-ChildItem | Measure-Object -Sum Length).Sum / 1GB
        $opsSinceMeasurement = 0

        Write-Host "Directory '$dirPath' is currently $currSize GB , and must become less than $limitGb GB..."
    }
}

Write-Host "Directory '$dirPath' has been reduced to $currSize GB."

